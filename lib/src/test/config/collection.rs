use crate::config::collection::*;
use crate::test::RESOURCES_DIR;

#[test]
fn config_deserealization_ok() {
    let path = RESOURCES_DIR() + "/collection.yaml";
    let coll = read_config(path);
    println!("{:#?} ", coll);
}

#[test]
fn collection_to_map_ok() {
    let path = RESOURCES_DIR() + "/collection.yaml";
    let coll = read_config(path);
    let map = coll.id_map();
    println!("{:#?} ", map);
}

#[test]
fn get_request_by_id() {
    let path = RESOURCES_DIR() + "/collection.yaml";
    let coll = read_config(path);

    let req = coll.id_map().get(&1).unwrap();
    println!("{:#?}", req)
}
