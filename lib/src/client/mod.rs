use std::{collections::HashMap, str::FromStr};

use crate::config::request::*;
use reqwest::{self, header::*, Client};

#[derive(Debug)]
struct Response {
    body: String,
    status_code: u16,
    headers: HashMap<String, String>,
}

impl Request {
    async fn run(&self) -> Result<Response, String> {
        let res: Result<reqwest::Response, reqwest::Error> = Client::new()
            .request(self.verb.to_method(), &self.url)
            .headers(self.make_headers())
            .send()
            .await;
        match res {
            Ok(response) => {
                let status_code: u16 = response.status().as_u16();
                let mut headers: HashMap<String, String> = HashMap::new();
                for (k, v) in response.headers() {
                    headers.insert(k.to_string(), v.to_str().unwrap().to_string());
                }
                let body: String = response.text().await.unwrap();
                return Ok(Response {
                    body,
                    status_code,
                    headers,
                });
            }
            Err(err) => {
                eprintln!("{:#?}", err);
                return Err(err.to_string());
            }
        }
    }

    fn make_headers(&self) -> HeaderMap {
        let mut headers = HeaderMap::new();
        match &self.headers {
            Some(map) => {
                for (k, v) in map {
                    headers.insert(
                        HeaderName::from_str(k.as_str()).unwrap(),
                        HeaderValue::from_str(v.as_str()).unwrap(),
                    );
                }
            }
            None => {}
        }
        return headers;
    }
}

#[cfg(test)]
macro_rules! aw {
    ($e:expr) => {
        tokio_test::block_on($e)
    };
}

#[test]
fn hello() {
    let req = Request::new(
        0,
        "https://httpbin.org/get".to_string(),
        Verb::GET,
        None,
        None,
    );
    println!("{:#?}", req);
    let result = aw!(req.run());
    println!("{:#?}", result.unwrap())
}
