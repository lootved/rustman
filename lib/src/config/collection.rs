use super::request::*;
use serde::{Deserialize, Serialize};

use std::collections::HashMap;

pub fn read_config(path: String) -> Collection {
    let f = std::fs::File::open(path).ok().unwrap();
    let coll: Collection = serde_yaml::from_reader(f).ok().unwrap();
    return coll.init();
}

#[derive(Debug, Serialize, Deserialize)]
pub struct Collection {
    pub meta: Metadata,
    pub data: Vec<RequestOrItems>,
    #[serde(skip_deserializing, skip_serializing)]
    id_to_path_to_request: HashMap<u32, Vec<u32>>,
}

#[derive(Debug, Serialize, Deserialize)]
pub struct Metadata {
    pub name: String,
    pub version: u32,
}

impl Collection {
    fn init(mut self) -> Self {
        self.id_to_path_to_request = self.map_path_to_request();
        return self;
    }

    pub fn access_request(&self, idxs: Vec<u32>) -> Option<&RequestOrItems> {
        let mut reqs = &self.data;
        let length = idxs.len();
        for idx in idxs {
            match reqs[idx as usize].items.as_ref() {
                Some(items) => reqs = items,
                None => {}
            }
        }
        return reqs.get(length - 1);
    }

    pub fn id_map(&self) -> &HashMap<u32, Vec<u32>> {
        return &self.id_to_path_to_request;
    }

    fn map_path_to_request(&self) -> HashMap<u32, Vec<u32>> {
        let mut res: HashMap<u32, Vec<u32>> = HashMap::new();

        for (pos, e) in self.data.iter().enumerate() {
            let idx: Vec<u32> = vec![pos.try_into().unwrap()];
            Self::add_request_or_items_to_map(e, idx, &mut res);
        }

        return res;
    }

    fn add_request_or_items_to_map(
        req_or_items: &RequestOrItems,
        indexes: Vec<u32>,
        res: &mut HashMap<u32, Vec<u32>>,
    ) {
        match req_or_items.items.as_ref() {
            Some(items) => {
                for (pos, e) in items.iter().enumerate() {
                    let mut idx_result = indexes.to_vec();
                    idx_result.push(pos.try_into().unwrap());
                    Self::add_request_or_items_to_map(e, idx_result, res);
                }
            }
            None => match &req_or_items.request {
                Some(request) => {
                    res.insert(request.id, indexes);
                }
                None => {}
            },
        }
    }
}
