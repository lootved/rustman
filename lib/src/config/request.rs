use reqwest::Method;
use serde::{Deserialize, Serialize};
use std::collections::HashMap;

#[derive(Clone, Debug, Serialize, Deserialize)]
pub struct RequestOrItems {
    pub name: String,
    pub headers: Option<HashMap<String, String>>,
    #[serde(flatten)]
    pub request: Option<Request>,
    pub items: Option<Vec<RequestOrItems>>,
}
#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct Request {
    pub id: u32,
    pub url: String,
    pub verb: Verb,
    pub body: Option<String>,
    pub headers: Option<HashMap<String, String>>,
}

#[derive(Debug, Serialize, Deserialize, Clone)]
pub enum Verb {
    #[serde(alias = "post")]
    POST,
    #[serde(alias = "get")]
    GET,
}

impl Verb {
    pub fn to_method(&self) -> reqwest::Method {
        match self {
            Self::POST => return Method::POST,
            Self::GET => return Method::GET,
        }
    }
}

impl Request {
    pub fn new(
        id: u32,
        url: String,
        verb: Verb,
        body: Option<String>,
        headers: Option<HashMap<String, String>>,
    ) -> Self {
        Self {
            id,
            url,
            verb,
            body,
            headers,
        }
    }
}

impl RequestOrItems {
    // create map id -> path to request given as a list of indexes
    pub fn map_path_to_request(&self) -> HashMap<u32, Vec<u32>> {
        let mut res: HashMap<u32, Vec<u32>> = HashMap::new();
        let idx: Vec<u32> = vec![];

        Self::add_request_or_items_to_map(&self, idx, &mut res);

        return res;
    }

    fn add_request_or_items_to_map(
        req_or_items: &RequestOrItems,
        indexes: Vec<u32>,
        res: &mut HashMap<u32, Vec<u32>>,
    ) {
        match req_or_items.items.as_ref() {
            Some(items) => {
                for (pos, e) in items.iter().enumerate() {
                    let mut idx_result = indexes.to_vec();
                    idx_result.push(pos.try_into().unwrap());
                    Self::add_request_or_items_to_map(e, idx_result, res);
                }
            }
            None => match &req_or_items.request {
                Some(request) => {
                    let mut idx_result = indexes.to_vec();
                    idx_result.push(request.id);
                    res.insert(request.id, idx_result);
                }
                None => {}
            },
        }
    }
}
