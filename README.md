# Rustman

## Description
httpclient based on `collections`, inspired by Postman.

## Objectives
- run http calls to Json REST endpoints from a cli/ui based on a reusable config
- support for variables and environments
- config can be stored in a git repos
- config must be easy to review during PR
- tool must be able to merge a `public` config with `private` one
- secrets stored in a `private` config
- same model for `public` and `public` config
